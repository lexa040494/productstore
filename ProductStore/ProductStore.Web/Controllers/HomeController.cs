﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProductStore.Bll.Interfaces;
using ProductStore.Model.Bll;

namespace ProductStore.Web.Controllers
{
    public class HomeController : Controller
    {
        private IProductService productService;
        private ICategoryService categoryService;

        public HomeController(IProductService productService, ICategoryService categoryService)
        {
            this.categoryService = categoryService;
            this.productService = productService;
        }

        public ActionResult Index()
        {
            
            return View();
        }
    }
}