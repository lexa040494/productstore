﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProductStore.Model.Bll;
using ProductStore.Model.Dal;

namespace ProductStore.Bll.Interfaces
{
    public interface ICategoryService
    {
        IEnumerable<CategoryDto> GelAllCategories();
        void CreateCategory(CategoryDto categoryDto);
    }
}
