﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProductStore.Model.Bll;

namespace ProductStore.Bll.Interfaces
{
    public interface IProductService
    {
        IEnumerable<ProductDto> GetProductsByCategory(int categoryId);
    }
}
