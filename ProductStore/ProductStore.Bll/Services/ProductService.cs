﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ProductStore.Bll.Interfaces;
using ProductStore.Dal.Interfaces;
using ProductStore.Model.Bll;
using ProductStore.Model.Dal;

namespace ProductStore.Bll.Services
{
    public class ProductService : IProductService
    {
        IUnitOfWork Database { get; set; }

        public ProductService(IUnitOfWork unitOfWork)
        {
            Database = unitOfWork;
        }

        public IEnumerable<ProductDto> GetProductsByCategory(int categoryId)
        {
            IQueryable<Product> products =
                Database.ProductRepository.GetAll().Where(x => x.CategoryId == categoryId).AsQueryable();

            return Mapper.Map<IQueryable<Product>, IQueryable<ProductDto>>(products);
        }
    }
}
