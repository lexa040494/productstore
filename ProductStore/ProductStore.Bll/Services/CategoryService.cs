﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ProductStore.Bll.Interfaces;
using ProductStore.Dal.Interfaces;
using ProductStore.Model.Bll;
using ProductStore.Model.Dal;

namespace ProductStore.Bll.Services
{
    public class CategoryService : ICategoryService
    {
        IUnitOfWork Database { get; set; }

        public CategoryService(IUnitOfWork unitOfWork)
        {
            Database = unitOfWork;
        }

        public IEnumerable<CategoryDto> GelAllCategories()
        {
            IEnumerable<Category> categories =
                (IEnumerable<Category>) Database.CategoryRepository.GetAll();

            return Mapper.Map<IEnumerable<Category>, IEnumerable<CategoryDto>>(categories);
        }

        public void CreateCategory(CategoryDto categoryDto)
        {
            var category = Mapper.Map<CategoryDto, Category>(categoryDto);

            Database.CategoryRepository.Create(category);
        }
    }
}
