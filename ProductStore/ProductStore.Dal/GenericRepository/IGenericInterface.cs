﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductStore.Dal.GenericRepository
{
    public interface IGenericInterface<T> where T : class
    {
        IEnumerable<T> GetAll();
        T GetbyId(int id);
        void Create(T obj);
        bool Update(T obj, int key);
        bool Delete(int key);
    }
}
