﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProductStore.Dal.Entities;

namespace ProductStore.Dal.GenericRepository
{
    public class GenericRepository<T> where T:class
    {
        private ProductStoreContext context;

        public GenericRepository(ProductStoreContext context)
        {
            this.context = context;
        }

        public IEnumerable<T> GetAll()
        {
            IEnumerable<T> collection = context.Set<T>().ToList();

            return collection;
        }

        public T GetbyId(int id)
        {
            T obj = context.Set<T>().Find(id);

            return obj;
        }

        public void Create(T obj)
        {
            context.Set<T>().Add(obj);
            context.SaveChanges();
        }

        public bool Update(T obj, int key)
        {
            T objExisting = context.Set<T>().Find(key);

            if (objExisting != null)
            {
                context.Entry(objExisting).CurrentValues.SetValues(obj);
                context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool Delete(int key)
        {
            T objExisting = context.Set<T>().Find(key);
            if (objExisting != null)
            {
                context.Set<T>().Remove(objExisting);
                context.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
