﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProductStore.Dal.GenericRepository;
using ProductStore.Model.Dal;

namespace ProductStore.Dal.Interfaces
{
    public interface ICategoryRepository : IGenericInterface<Category>
    {
    }
}
