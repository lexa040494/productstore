﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProductStore.Dal.Repositories;

namespace ProductStore.Dal.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IProductRepository ProductRepository { get; }
        ICategoryRepository CategoryRepository { get; }
        IProductAttributeRepository ProductAttributeRepository { get; }
        IProfileManager ProfileManager { get; }
        StoreUserManager StoreUserManager { get; }
        StoreRoleManager StoreRoleManager { get; }
    }
}
