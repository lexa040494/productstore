﻿using ProductStore.Dal.Entities;
using ProductStore.Dal.GenericRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProductStore.Dal.Interfaces;
using ProductStore.Model.Dal;

namespace ProductStore.Dal.Repositories
{
    public class UserProfileRepository : GenericRepository<UserProfile>, IProfileManager
    {
        public UserProfileRepository(ProductStoreContext context) : base(context)
        {
        }
    }
}
