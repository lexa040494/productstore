﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProductStore.Dal.Entities;
using ProductStore.Dal.Interfaces;
using ProductStore.Dal.GenericRepository;
using ProductStore.Model.Dal;

namespace ProductStore.Dal.Repositories
{
    public class CategoryRepository : GenericRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(ProductStoreContext context) : base(context)
        {
        }
    }
}
