﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProductStore.Dal.Entities;
using ProductStore.Dal.Interfaces;

namespace ProductStore.Dal.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private ProductStoreContext context;
        private ProductRepository productRepository;
        private CategoryRepository categoryRepository;
        private ProductAttributeRepository productAttributeRepository;
        private UserProfileRepository userProfileRepository;
        private StoreUserManager storeUserManager;
        private StoreRoleManager storeRoleManager;

        public UnitOfWork(string connectionString)
        {
            context = new ProductStoreContext(connectionString);
        }

        public IProductRepository ProductRepository
        {
            get { return productRepository ?? (productRepository = new ProductRepository(context)); }
        }

        public ICategoryRepository CategoryRepository
        {
            get { return categoryRepository ?? (categoryRepository = new CategoryRepository(context)); }
        }

        public IProductAttributeRepository ProductAttributeRepository
        {
            get { return productAttributeRepository ?? (productAttributeRepository = new ProductAttributeRepository(context)); }
        }

        public IProfileManager ProfileManager
        {
            get { return userProfileRepository ?? (userProfileRepository = new UserProfileRepository(context)); }
        }

        public StoreUserManager StoreUserManager
        {
            get { return storeUserManager; }
        }

        public StoreRoleManager StoreRoleManager
        {
            get { return storeRoleManager; }
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        
    }
}
