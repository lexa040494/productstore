﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using ProductStore.Model.Dal;

namespace ProductStore.Dal.Repositories
{
    public class StoreRoleManager : RoleManager<StoreRole>
    {
        public StoreRoleManager(IRoleStore<StoreRole> store) : base(store)
        {
        }
    }
}
