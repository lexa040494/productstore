﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using ProductStore.Model.Dal;

namespace ProductStore.Dal.Repositories
{
    public class StoreUserManager : UserManager<StoreUser>
    {
        public StoreUserManager(IUserStore<StoreUser> store) : base(store)
        {
        }
    }
}
