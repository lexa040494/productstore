﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using ProductStore.Model.Dal;

namespace ProductStore.Dal.Entities
{
    public class ProductStoreContext : DbContext
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<ProductAttribute> ProductAttributes { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }

        static ProductStoreContext() 
        {
            System.Data.Entity.Database.SetInitializer<ProductStoreContext>(new StoreInitializer());
        }

        public ProductStoreContext(string connectionString) : base(connectionString)
        {
        }
    }
}
