﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using ProductStore.Model.Dal;

namespace ProductStore.Dal.Entities
{
    public class StoreInitializer : DropCreateDatabaseIfModelChanges<ProductStoreContext>
    {
        protected override void Seed(ProductStoreContext db)
        {
            db.Categories.Add(new Category { CategoryName = "Бытовая техника"});
            db.Categories.Add(new Category { CategoryName = "Компьютеры и ноутбуки" });
            db.Categories.Add(new Category { CategoryName = "Одежда" });
            db.Products.Add(new Product { Name = "Product1", ImagePath = "", Price = 10, CategoryId = 1});
            db.SaveChanges();
        }
    }
}
