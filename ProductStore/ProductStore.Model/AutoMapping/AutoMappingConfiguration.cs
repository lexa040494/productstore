﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ProductStore.Model.Bll;
using ProductStore.Model.Dal;

namespace ProductStore.Model.AutoMapping
{
    public class AutoMappingConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<Product, ProductDto>()
                    .ForMember(d => d.Id, opts => opts.MapFrom(src => src.Id))
                    .ForMember(d => d.Name, opts => opts.MapFrom(src => src.Name))
                    .ForMember(d => d.Price, opts => opts.MapFrom(src => src.Price))
                    .ForMember(d => d.ImagePath, opts => opts.MapFrom(src => src.ImagePath))
                    .ForMember(d => d.CategoryId, opts => opts.MapFrom(src => src.CategoryId));
                x.CreateMap<ProductDto, Product>()
                    .ForMember(d => d.Id, opts => opts.MapFrom(src => src.Id))
                    .ForMember(d => d.Name, opts => opts.MapFrom(src => src.Name))
                    .ForMember(d => d.Price, opts => opts.MapFrom(src => src.Price))
                    .ForMember(d => d.ImagePath, opts => opts.MapFrom(src => src.ImagePath))
                    .ForMember(d => d.CategoryId, opts => opts.MapFrom(src => src.CategoryId));
                x.CreateMap<Category, CategoryDto>()
                    .ForMember(d => d.Id, opts => opts.MapFrom(src => src.Id))
                    .ForMember(d => d.CategoryName, opts => opts.MapFrom(src => src.CategoryName));
                x.CreateMap<CategoryDto, Category>()
                    .ForMember(d => d.Id, opts => opts.MapFrom(src => src.Id))
                    .ForMember(d => d.CategoryName, opts => opts.MapFrom(src => src.CategoryName));
                x.CreateMap<ProductAttribute, ProductAttributeDto>()
                    .ForMember(d => d.Id, opts => opts.MapFrom(src => src.Id))
                    .ForMember(d => d.AttributeName, opts => opts.MapFrom(src => src.AttributeName))
                    .ForMember(d => d.AttributeValue, opts => opts.MapFrom(src => src.AttributeValue))
                    .ForMember(d => d.ProductId, opts => opts.MapFrom(src => src.ProductId));
                x.CreateMap<ProductAttributeDto, ProductAttribute>()
                    .ForMember(d => d.Id, opts => opts.MapFrom(src => src.Id))
                    .ForMember(d => d.AttributeName, opts => opts.MapFrom(src => src.AttributeName))
                    .ForMember(d => d.AttributeValue, opts => opts.MapFrom(src => src.AttributeValue))
                    .ForMember(d => d.ProductId, opts => opts.MapFrom(src => src.ProductId));
            });
        }
    }
}
